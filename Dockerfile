FROM openjdk:8-alpine
ADD target/zuul-service*.jar /app/zuul-service.jar
EXPOSE 8080
ENTRYPOINT [ "/usr/bin/java", "-Xms512m", "-Xmx512m", "-Xss256k", \
 "-XX:MetaspaceSize=64m", "-XX:MaxMetaspaceSize=128m", "-XX:ReservedCodeCacheSize=32M", \
 "-XX:CodeCacheMinimumFreeSpace=2M", "-XX:CompressedClassSpaceSize=16M", \
 "-XX:MinMetaspaceExpansion=1M", "-XX:MaxMetaspaceExpansion=8M", "-XX:CodeCacheExpansionSize=256k", \
 "-Djava.security.egd=file:/dev/./urandom", "-Dfile.encoding=UTF-8", "-Duser.home=/", \
 "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005", \
 "-jar", "/app/zuul-service.jar"]
